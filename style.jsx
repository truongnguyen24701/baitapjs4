
//bài 1
document.getElementById("btn-ket-qua").addEventListener("click", function () {
    var diemChuanValue = document.getElementById("txt-diem").valueAsNumber;
    var khuVucValue = Number(document.getElementById("inputGroupSelect01").value);
    var doiTuongValue = Number(document.getElementById("inputGroupSelect02").value);
    var diem1Value = document.getElementById("txt-1").valueAsNumber;
    var diem2Value = document.getElementById("txt-2").valueAsNumber;
    var diem3Value = document.getElementById("txt-3").valueAsNumber;

    var diemTong = khuVucValue + doiTuongValue + diem1Value + diem2Value + diem3Value;
    console.log({ diemTong, diemChuanValue, khuVucValue, doiTuongValue, diem1Value, diem2Value, diem3Value });

    if (diemChuanValue < diemTong) {
        console.log("Bạn đã đậu");
        document.getElementById("resultKetQua").innerHTML = `<span>Bạn đã đậu. Tổng điểm ${diemTong} </span>`
    } else {
        console.log("Bạn đã rớt");
        document.getElementById("resultKetQua").innerHTML = `<span>Bạn đã rớt. Tổng điểm: ${diemTong} </span>`
    }

});



//bài 2
document.getElementById("btn-tien-dien").addEventListener("click", function () {
    var hoTenValue = document.getElementById("txt-ho-ten").value;
    var soKwValue = document.getElementById("txt-so-kw").value;

    var tienDien = 0;
    console.log({ hoTenValue, soKwValue });

    var giaTienKwDau = 500;
    var giaTienKw50to100 = 650;
    var giaTienKw100to200 = 850;
    var giaTienKw200to350 = 1100;
    var giaTienKw350up = 1300;


    //tổng số tiền 
    if (soKwValue <= 50) {
        tienDien = soKwValue * giaTienKwDau;
    } else if (soKwValue <= 100) {
        tienDien = giaTienKwDau * 50 + (soKwValue - 50) * giaTienKw50to100;
    } else if (soKwValue <= 200) {
    } else if (soKwValue <= 350) {
        tienDien = giaTienKwDau * 50 + 50 * giaTienKw50to100 + 100 * giaTienKw100to200 + (soKwValue - 200) * giaTienKw200to350;
    } else {
        tienDien = giaTienKwDau * 50 + 50 * giaTienKw50to100 + 100 * giaTienKw100to200 + 150 * giaTienKw200to350 + (soKwValue - 350) * giaTienKw350up;
    }
    document.getElementById("resultTienDien").innerHTML = `<span>Họ tên: ${hoTenValue}, Tiền điện: ${tienDien} `;
    console.log({ tienDien });
});

